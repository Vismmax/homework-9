A microservice architecture was chosen. Since by increasing the granularity of modules, the architecture aims to reduce the degree of entanglement and increase the connectivity, which makes it easier to add and change functions in the system at any time.

Http was chosen as the data exchange protocol, since receiving and sending data in real time is not needed. But it is possible to connect WebSocket if there is a need for chats, for example, between trainers and visitors.

The root folder contains the environment configuration files for development. And also the file is the entry point to the application, in which we connect and run express, check the connection to the database, and also make additional connections and settings.