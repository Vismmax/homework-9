Files containing queries to the database.
Base repository file that accepts the model and describes the base methods used by all entities.
Extended Query files based on entity.