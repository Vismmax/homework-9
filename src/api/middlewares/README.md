Files with middlewares.
- Authentication handling
- Authorization processing
- Registration processing
- Error processing
- Saving pictures to a third-party service (if necessary)
- JWT
- Processing of sent data
- etc.