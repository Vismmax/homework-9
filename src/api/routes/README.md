The index file is the entry point to the routes, here we add controllers aggregated by entities. In separate files there are the controllers themselves:
- authentication
- work with users
- work with staff
- work with the bar
- work with payment
- work with equipment
- work with workouts
- blog