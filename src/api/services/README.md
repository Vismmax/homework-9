Files with the services for:
- authentication
- work with users
- work with staff
- work with the bar
- work with payment
- work with equipment
- work with workouts
- work with email
- work with pictures
- blog
- etc.
They create requests to the repository, other third-party services, or perform other necessary actions with the data.